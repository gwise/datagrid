﻿using demo.Models;
using demo.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace demo.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class demoView : ContentPage
	{
        public demoView ()
		{
            InitializeComponent();
        }

        private void btnSearch_Clicked(object sender, EventArgs e)
        {
            (this.BindingContext as demoViewModel).Search();
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            Console.WriteLine("a");
            //this.datagrid.SelectedRowIndex;

            //헤더 컬럼 클릭 할 경우 정렬
            (this.BindingContext as demoViewModel).Sorting("BarcodeText");

        }
    }
}