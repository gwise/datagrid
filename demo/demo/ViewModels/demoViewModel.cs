﻿using demo.Models;
using demo.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace demo.ViewModels
{
    public class demoViewModel : ViewModelBase 
    {
        private ObservableCollection<object> _searchResult = new ObservableCollection<object>();

        public List<demoModel> lstModel;

        public ICommand TapCommand { get; }

        public demoViewModel()
        {
            TapCommand = new Command(Tapped);
        }

        private void Tapped()
        {
            Console.WriteLine("a");
        }

        public void Search()
        {
            lstModel = new List<demoModel>();

            for (int i = 0; i < 100; i++)
            {
                lstModel.Add(new demoModel { BarcodeText = i.ToString(), BarcodeType = "Code128" });
            }

            //SearchResult = new ObservableCollection<object>(lstModel.ToList<object>());
            SearchResult.Clear();
            SearchResult = new ObservableCollection<object>(lstModel.OrderBy(x=>x.BarcodeText).ToList<object>());
        }

        public void Sorting(string column)
        {
            SearchResult.Clear();
            //SearchResult = null;
            SearchResult = new ObservableCollection<object>(lstModel.OrderByDescending(x=>x.BarcodeText).ToList<object>());
        }

        public ObservableCollection<object> SearchResult { get => _searchResult; set => SetProperty(ref this._searchResult, value); }
    }
}

