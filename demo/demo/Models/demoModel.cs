﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace demo.Models
{
    public class demoModel : BindableObject
    {
        private string _barcodeType;

        public string BarcodeType 
        {
            get { return this._barcodeType; }
            set
            {
                if (_barcodeType == value) return;
                this._barcodeType = value;
                OnPropertyChanged("BarcodeType");
            }
        }
        public string BarcodeText { get; set; }
    }
}
